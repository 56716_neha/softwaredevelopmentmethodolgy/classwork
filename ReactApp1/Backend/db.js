// const mysql = require('mysql2')

// const openConnection =( ) =>{
//     const connection = mysql.createConnection({
//         uri: 'mysql://db:3306',
//         user: 'root',
//         password:'manager',
//         database: 'Demodb',
         
//     });
//     connection.connect();
//     return connection;
// }


// module.exports = {
//     openConnection
// }

const mysql = require('mysql2')

// connection pool
const pool = mysql.createPool({
    host = 'localhost',
    user = 'root',
    password ='manager',
    database = 'Demodb',
    waitForConnections: true,
    connectionLimit: 10,
    queueLimit: 0,
})

module.exports = pool