CREATE TABLE Product (
    Id INTEGER PRIMARY KEY auto_increment,
    Title VARCHAR(200),
    Description VARCHAR(300),
    Price FLOAT,
    categoryId INTEGER
);


CREATE TABLE Category (
    Id INTEGER PRIMARY KEY auto_increment,
    Title VARCHAR(200),
    Description VARCHAR(300),
);

INSERT INTO Category (Title, Description) VALUES ('Electronics', 'Electronics products');
INSERT INTO Category (Title, Description) VALUES ('Computers', 'List of Computers');
INSERT INTO Category (Title, Description) VALUES ('TV', 'Television');
INSERT INTO Category (Title, Description) VALUES ('Gifts', 'Gifts');
INSERT INTO Category (Title, Description) VALUES ('Fasion', 'Men and Women acessories');
